FROM python:3.5-alpine3.11

WORKDIR /var/app

COPY . ./

CMD [ "python", "start.py"]
 
